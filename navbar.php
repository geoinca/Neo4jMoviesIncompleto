  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">GGVD VIDEOCLUB</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="index.php">Movie <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="actor.php">Actor</a>
      <a class="nav-item nav-link" href="filmography.php">Filmography</a>
      <a class="nav-item nav-link " href="useOfTheAPI.php">API</a>
    </div>
  </div>
</nav>
